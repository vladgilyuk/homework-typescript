import { Role, StudyField, Sex, Nationality, Degree, Result } from '../enums.js';
import { Person, Diploma, RedDiploma } from '../interfaces.js';
import { CheckUniversityEnrollment} from '../decorators.js';
import { Professor } from './Professor';

const getId = (() => {
    let counter = 0;
    return () => counter++;
  })();

class Student implements Required<Person> {
    constructor(
        public name: string,
        public age: number,
        public studyField: StudyField,
        public nationality: Nationality,
        public sex: Sex,
        public university: string | null = null,
        public midExamAnswer: number | null = null,
        public finalExamAnswer: number | null = null,
        public extraExamAnswer: number | null = null,
        public books: number = 0,
        public librarianDebt: number = 0,
        public hasPhoneOnExam: boolean = false,
        public foundPhoneOnExam: boolean = false,
        public midExam: boolean = false,
        public finalExam: boolean = false, 
        public extraExam: boolean = false,
        public diploma: Diploma | null = null, 
        public redDiploma: RedDiploma | null = null,
        public degree: Degree = Degree.None) {}

    @CheckUniversityEnrollment
    passMidExam(answer: number): void {
        this.midExamAnswer = answer;
        console.log(`${Result.Success} ${Role.Student} ${this.name} added Mid Exam answer: ${JSON.stringify(answer)}`);
    }
      
    @CheckUniversityEnrollment
    passFinalExam(answer: number): void {
        if (this.midExamAnswer > 0 && this.midExam === false) {
              console.log(`${Result.Failed} ${Role.Student} ${this.name}, should wait until the Mid exam is checked by ${Role.Professor}`)   
        } else {
            if (this.midExam) {
              this.finalExamAnswer = answer;
              console.log(`${Result.Success} ${Role.Student} ${this.name} added Final Exam answer: ${JSON.stringify(answer)}`);
            } else {
              console.log(`${Result.Failed} ${Role.Student}  ${this.name}, should pass the Mid Exam first`);
            }
        }
    }
      
    @CheckUniversityEnrollment
    passExtraExam(answer: number): void {
        if (this.finalExamAnswer > 0 && this.finalExam === false) {
          console.log(`${Result.Failed} ${Role.Student} ${this.name}, should wait until the Final exam is checked by ${Role.Professor}`)
        } else {
            if (this.finalExam) {
                this.extraExamAnswer = answer;
                console.log(`${Result.Success} ${Role.Student} ${this.name} added Extra Exam answer: ${JSON.stringify(answer)}`);
            } else {
                console.log(`${Result.Failed} ${Role.Student} ${this.name}, should pass the Final Exam first`);
             }
        }
    }

    @CheckUniversityEnrollment
    payBribe(professor: Professor): never {
      const bribe: unknown = Math.random() < 0.5 ? "alcohol" : "money";
        console.log(`Bribe in a form of ${bribe} was suggested to ${Role.Professor} ${professor.name} by ${Role.Student} ${this.name}`);
        throw new Error(`${Result.Failed} ${Role.Student} ${this.name} should remember that bribe - is not an accepted way to pass an exam!`);
    }


    @CheckUniversityEnrollment
    getDegree(): void {
        if (this.midExam && this.finalExam && this.extraExam) {
            if (this.degree === Degree.None) {
                this.degree = Degree.Bachelor;
            } else if (this.degree === Degree.Bachelor) {
                this.degree = Degree.Master;
            } else {
                this.degree = Degree.Doctorate;
            }
            this.redDiploma = {
                id: getId(),
                field: this.studyField,
                holder: this.name,
                specialNote: `${Role.Student} ${this.name} is an extremely talented ${Role.Student}`
            };
            this.midExam = false;
            this.finalExam = false;
            this.extraExam = false;
            this.midExamAnswer = null;
            this.finalExamAnswer = null;
            this.extraExamAnswer = null;
            console.log(`${Result.Success} ${Role.Student} ${this.name}, congratulations on obtaining a RED DIPLOMA in ${this.studyField} and obtaining a ${this.degree}`);
        } else if (this.midExam && this.finalExam) {
            if (this.degree === Degree.None) {
                this.degree = Degree.Bachelor;
            } else if (this.degree === Degree.Bachelor) {
                this.degree = Degree.Master;
            } else {
                this.degree = Degree.Doctorate;
            }
            this.diploma = {
              id:  getId(),
              field: this.studyField,
              holder: this.name,
            };
            this.midExam = false;
            this.finalExam = false;
            this.extraExam = false;
            this.midExamAnswer = null;
            this.finalExamAnswer = null;
            this.extraExamAnswer = null;
            console.log(`${Result.Success} ${Role.Student} ${this.name}, congratulations on obtaining a diploma in ${this.studyField} and obtaining a ${this.degree}`);
        } else {
            console.log(`${Result.Failed} ${Role.Student} ${this.name}, should pass at least 2 exams(Mid & Final) in order to get a diploma and a degree`);
        }
    }
}  

export { Student }