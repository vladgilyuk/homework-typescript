import { Role, Degree, Result } from '../enums.js';
import { Employee } from './Employee.js';
import { Professor } from './Professor.js';
import { Librarian } from './Librarian.js';
import { Rector } from './Rector.js';
import { Student } from './Student.js';

class Guard extends Employee {
    public university: string | null = null;
    private _foundPhones: number = 0;
    private _checkedPhones: number = 0;
    private _totalCheckedPhones: number = 0;
    private _salary: number = 1000;
    private _receivedSalary: number = 0;
    constructor(name: string, age: number) {
        super(name, age, Role.Guard, Degree.None);
    }

    checkRandomPerson(person: Student | Professor | Librarian | Rector) {
        if (person.university === this.university) {
            if (this.isStudent(person)) {
                if (person.hasPhoneOnExam) {
                    this._foundPhones++;
                    this._checkedPhones++;
                    person.foundPhoneOnExam = true;
                    console.log(`${Result.Success} ${Role.Guard} ${this.name} found the phone in ${Role.Student} ${person.name}`);
                } else {
                    this._checkedPhones++;
                    console.log(`${Result.Success} ${Role.Student} ${person.name} has successfully passed a check by ${Role.Guard} ${this.name}`);
                }
            } else if (this.isProfessor(person) || this.isLibrarian(person) || this.isRector(person) ) {
                console.log(`${Result.Failed} ${person.role} ${person.name}, has been mistaken for a ${Role.Student} by ${Role.Guard} ${this.name}.`);
            } else {
                console.log(`${Result.Failed} unknown person`);
            }
        } else {
            console.log(`${Result.Failed} University mismatch`)
        }
    }
  
    // Type Guard "is" & Type Operator "instanceof"
    isStudent(person: any): person is Student {
        return person instanceof Student;
    }
    
    isProfessor(person: any): person is Professor {
        return person instanceof Professor;
    }

    isLibrarian(person: any): person is Librarian {
        return person instanceof Librarian;
    }
    
    isRector(person: any): person is Rector {
        return person instanceof Rector;
    }
  
    getSalary(): void {
        if (this._checkedPhones > 2) {
            console.log(`Currently ${Role.Guard} ${this.name} has checked ${this._checkedPhones} phones`)
            this._receivedSalary += this._salary;
            console.log(`${Result.Success} ${Role.Guard} ${this.name} has received ${this._salary} dollars, totaly received ${this._receivedSalary}`)
            this._totalCheckedPhones += this._checkedPhones;
            console.log(`Currently ${Role.Guard} ${this.name} has checked ${this._totalCheckedPhones} phones in Total`)
            this._checkedPhones = 0;
        } else {
            console.log(`${Result.Failed} ${Role.Guard} ${this.name}, should check more than 2 phones to get paid`)
            console.log(`Currently ${Role.Guard} ${this.name} has checked ONLY ${this._checkedPhones} phones`)
        }
    }

    getPromotion(): void {
        if (this._totalCheckedPhones > 3 || this._foundPhones > 2) {
            console.log(`Currently ${Role.Guard} ${this.name} has checked ${this._totalCheckedPhones} phones in Total and found ${this._foundPhones}`)
            this._salary += 1000;
            console.log(`${Result.Success} ${Role.Guard} ${this.name} has been promoted, the current base salary is ${this._salary}`)
            this._totalCheckedPhones = 0;
            this._foundPhones = 0;
        } else {
            console.log(`${Result.Failed} ${Role.Guard} ${this.name} should check more than 3 phones in Total or find more than 2 phones to get promoted`)
            console.log(`Currently ${Role.Guard} ${this.name} has checked ONLY ${this._totalCheckedPhones} phones in Total and found ONLY ${this._foundPhones}`)
        }
    }
}

export { Guard }