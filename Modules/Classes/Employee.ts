import { Role, Degree } from 'enums.js';
// Abstract Class
abstract class Employee {
    constructor(public name: string, protected age: number, public role: Role, public degree: Degree) {
    } 
     getRole(): string {
        return this.role
     };
     getDegree(): string {
        return this.degree
     };

    abstract getSalary(): void;
    abstract getPromotion(): void;
}

export { Employee }