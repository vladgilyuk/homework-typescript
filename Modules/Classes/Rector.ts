import { Role, StudyField, Degree, Result } from '../enums.js';
import { Employee } from './Employee.js';
import { Professor } from './Professor.js';

class Rector extends Employee {
    public university: string | null = null;
    private _receivedSalary: number = 0;
    private _conductedMeetings: number = 0;
    private _totalConductedMeetings: number = 0;
    private _salary: number = 5000;
    constructor(name: string, age: number) {
        super(name, age, Role.Rector, Degree.Doctorate);
    }
    
    conductMeeting(professors: Professor[]): void {
        let allProfessorsFromSameUniversity = true;

        professors.forEach((professor) => {
            if (professor.university !== this.university) {
                allProfessorsFromSameUniversity = false;
            }
        });
    
        if (allProfessorsFromSameUniversity) {
            console.log(`${Result.Success} Meeting by ${Role.Rector} ${this.name} has been held with the following professors:`);
            professors.forEach((professor) => {
                console.log(professor.name);
            });
            this._conductedMeetings++;
        } else {
            console.log(`${Result.Failed} Not all ${Role.Professor}S are from this university`);
        }
    }
  
    awardBestProfessor(professors: Professor[]): void {
        let maxRaisedBestStudent = 0;
        let bestProfessor: Professor | null = null;

        for (const professor of professors) {
            if (professor.university === this.university) {
                const raisedBestStudents = professor.getRaisedBestStudents();
                if (raisedBestStudents > maxRaisedBestStudent) {
                    maxRaisedBestStudent = raisedBestStudents;
                    bestProfessor = professor;
                }
            }
        }

        if (bestProfessor) {
            console.log(`${Result.Success} ${Role.Rector} ${this.name} awarded the ${Role.Professor} ${bestProfessor.name} for raising most Best Students`);
        } else {
            console.log(`${Result.Failed} No eligible professors found`);
        }
    }

    getSalary(): void {
        if (this._conductedMeetings > 1) {
            console.log(`Currently ${Role.Rector} ${this.name} has conducted ${this._conductedMeetings} meetings`)
            this._receivedSalary += this._salary;
            console.log(`${Result.Success} ${Role.Rector} ${this.name} has received ${this._salary} dollars, totaly received ${this._receivedSalary}`)
            this._totalConductedMeetings += this._conductedMeetings;
            this._conductedMeetings = 0;
            console.log(`Currently ${Role.Rector} ${this.name} has conducted ${this._totalConductedMeetings} meetings in Total`)
        } else {
            console.log(`${Result.Failed} ${Role.Rector} ${this.name}, should conduct more than 1 meeting to get paid`)
            console.log(`Currently ${Role.Rector} ${this.name} has conducted ONLY ${this._totalConductedMeetings} meetings`)
        }
    }
    getPromotion(): void {
        if (this._totalConductedMeetings > 2) {
            console.log(`Currently ${Role.Rector} ${this.name} has conducted ${this._totalConductedMeetings} meetings in Total`)
            this._salary += 2000;
            console.log(`${Result.Success} ${Role.Rector} ${this.name} has been promoted, the current base salary is ${this._salary}`)
            this._totalConductedMeetings = 0;
        } else {
            console.log(`${Result.Failed} ${Role.Rector} ${this.name}, should conduct more than 2 meetings in Total to get promoted`)
            console.log(`Currently ${Role.Rector} ${this.name} has conducted ONLY ${this._totalConductedMeetings} meetings`)
        }
    }
}

export { Rector }