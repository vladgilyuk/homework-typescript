import { StudyField } from 'enums.js';

// Interfaces
interface Person {
    name: string;
    age: number;
    nationality? :string,
    sex?: string;
}

interface EducationalInstitution {
    location: string;
    foundedYear: number;
}

interface Diploma {
    id: number;               
    field: StudyField;
    holder: string;
}

// Type Assertions
interface RedDiploma extends Diploma {
    specialNote: string;
}

export { 
    Person, 
    EducationalInstitution, 
    Diploma, 
    RedDiploma 
}