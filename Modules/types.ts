import { EducationalInstitution, Diploma, RedDiploma } from 'interfaces.js';
// Custom Types
type PublicUniversity = EducationalInstitution & {scolarship: number};
type PrivateUniversity = EducationalInstitution & {fee: number}

// Utility Types
type ReadonlyDiploma = Readonly<Diploma>;
type ReadonlyRedDiploma = Readonly<RedDiploma>;

export {
    PrivateUniversity,
    PublicUniversity,
    ReadonlyDiploma,
    ReadonlyRedDiploma
}