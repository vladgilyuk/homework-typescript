// Enums
enum Role {
    Professor = "PROFESSOR",  
    Rector = 'RECTOR',
    Librarian = 'LIBRARIAN',
    Guard = "GUARD",
    Student = "STUDENT"
}

enum StudyField {
    ComputerScience = "COMPUTER SCIENCE",
    Psychology = "PSYCHOLOGY",
    History = "HISTORY",
    Law = "LAW",
    Biology = "BIOLOGY",
    Physics = "PHYSICS",
    Chemistry = "CHEMISTRY",
    Economics = "ECONOMICS",
}

enum Sex {
    Male = "MALE",
    Female = "FEMALE",
}

enum Nationality {
    Ukrainian = "UKRAINIAN",
    Polish = "POLISH",
    German = "GERMAN",
    English = "ENGLISH",
    Chinese = "CHINESE",
    French = "FRENCH",
}

enum Degree {
    None = "NONE",
    Bachelor = "BACHELOR DEGREE",
    Master = "MASTER DEGREE",
    Doctorate = "DOCTORATE DEGREE",
}

enum Result {
    Failed = "FAILED!",
    Success = "SUCCESS!",
}

export {
    Role,
    StudyField,
    Sex,
    Nationality,
    Degree,
    Result
};