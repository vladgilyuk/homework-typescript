// Enums
var Role;
(function (Role) {
    Role["Professor"] = "PROFESSOR";
    Role["Rector"] = "RECTOR";
    Role["Librarian"] = "LIBRARIAN";
    Role["Guard"] = "GUARD";
    Role["Student"] = "STUDENT";
})(Role || (Role = {}));
var StudyField;
(function (StudyField) {
    StudyField["ComputerScience"] = "COMPUTER SCIENCE";
    StudyField["Psychology"] = "PSYCHOLOGY";
    StudyField["History"] = "HISTORY";
    StudyField["Law"] = "LAW";
    StudyField["Biology"] = "BIOLOGY";
    StudyField["Physics"] = "PHYSICS";
    StudyField["Chemistry"] = "CHEMISTRY";
    StudyField["Economics"] = "ECONOMICS";
})(StudyField || (StudyField = {}));
var Sex;
(function (Sex) {
    Sex["Male"] = "MALE";
    Sex["Female"] = "FEMALE";
})(Sex || (Sex = {}));
var Nationality;
(function (Nationality) {
    Nationality["Ukrainian"] = "UKRAINIAN";
    Nationality["Polish"] = "POLISH";
    Nationality["German"] = "GERMAN";
    Nationality["English"] = "ENGLISH";
    Nationality["Chinese"] = "CHINESE";
    Nationality["French"] = "FRENCH";
})(Nationality || (Nationality = {}));
var Degree;
(function (Degree) {
    Degree["None"] = "NONE";
    Degree["Bachelor"] = "BACHELOR DEGREE";
    Degree["Master"] = "MASTER DEGREE";
    Degree["Doctorate"] = "DOCTORATE DEGREE";
})(Degree || (Degree = {}));
var Result;
(function (Result) {
    Result["Failed"] = "FAILED!";
    Result["Success"] = "SUCCESS!";
})(Result || (Result = {}));
export { Role, StudyField, Sex, Nationality, Degree, Result };
