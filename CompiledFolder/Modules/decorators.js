import { Role, Result } from "./enums.js";
// This decorator check if the Student is Enrolled in the university
function CheckUniversityEnrollment(target, propertyKey, descriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args) {
        if (this.university !== null) {
            originalMethod.apply(this, args);
        }
        else {
            console.log(`${Result.Failed} ${Role.Student} ${this.name}, should enroll in the university first`);
        }
    };
    return descriptor;
}
// Other decorators log what the method is about to do (the result is shown in the method itself)
function logHire(target, propertyKey, descriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args) {
        const employee = args[0];
        let studyField = '';
        if (args.length > 1) {
            studyField = `for Study Field ${args[1]}`;
        }
        console.log(`Hiring ${employee.constructor.name} '${employee.name}' ${studyField}`);
        originalMethod.apply(this, args);
    };
    return descriptor;
}
function logFire(target, propertyKey, descriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args) {
        const employee = args[0];
        console.log(`Firing ${employee.constructor.name} '${employee.name}'`);
        originalMethod.apply(this, args);
    };
    return descriptor;
}
function logEnrollStudent(target, propertyKey, descriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args) {
        const [student, studyField] = args;
        console.log(`Enrolling '${Role.Student}' '${student.name}' for Study Field ${studyField}`);
        originalMethod.apply(this, args);
    };
    return descriptor;
}
function logKickStudent(target, propertyKey, descriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args) {
        const student = args[0];
        console.log(`Kicking '${Role.Student}' '${student.name}'`);
        originalMethod.apply(this, args);
    };
    return descriptor;
}
function logAddStudyField(target, propertyKey, descriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args) {
        const studyField = args[0];
        console.log(`Adding Study Field ${studyField}`);
        originalMethod.apply(this, args);
    };
    return descriptor;
}
function logDeleteStudyField(target, propertyKey, descriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args) {
        const studyField = args[0];
        console.log(`Deleting Study Field ${studyField}`);
        originalMethod.apply(this, args);
    };
    return descriptor;
}
export { CheckUniversityEnrollment, logHire, logFire, logEnrollStudent, logKickStudent, logAddStudyField, logDeleteStudyField };
