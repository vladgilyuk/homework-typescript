import { Role, Degree, Result } from '../enums.js';
import { Employee } from './Employee.js';
import { Professor } from './Professor.js';
import { Librarian } from './Librarian.js';
import { Rector } from './Rector.js';
import { Student } from './Student.js';
class Guard extends Employee {
    constructor(name, age) {
        super(name, age, Role.Guard, Degree.None);
        this.university = null;
        this._foundPhones = 0;
        this._checkedPhones = 0;
        this._totalCheckedPhones = 0;
        this._salary = 1000;
        this._receivedSalary = 0;
    }
    checkRandomPerson(person) {
        if (person.university === this.university) {
            if (this.isStudent(person)) {
                if (person.hasPhoneOnExam) {
                    this._foundPhones++;
                    this._checkedPhones++;
                    person.foundPhoneOnExam = true;
                    console.log(`${Result.Success} ${Role.Guard} ${this.name} found the phone in ${Role.Student} ${person.name}`);
                }
                else {
                    this._checkedPhones++;
                    console.log(`${Result.Success} ${Role.Student} ${person.name} has successfully passed a check by ${Role.Guard} ${this.name}`);
                }
            }
            else if (this.isProfessor(person) || this.isLibrarian(person) || this.isRector(person)) {
                console.log(`${Result.Failed} ${person.role} ${person.name}, has been mistaken for a ${Role.Student} by ${Role.Guard} ${this.name}.`);
            }
            else {
                console.log(`${Result.Failed} unknown person`);
            }
        }
        else {
            console.log(`${Result.Failed} University mismatch`);
        }
    }
    // Type Guard "is" & Type Operator "instanceof"
    isStudent(person) {
        return person instanceof Student;
    }
    isProfessor(person) {
        return person instanceof Professor;
    }
    isLibrarian(person) {
        return person instanceof Librarian;
    }
    isRector(person) {
        return person instanceof Rector;
    }
    getSalary() {
        if (this._checkedPhones > 2) {
            console.log(`Currently ${Role.Guard} ${this.name} has checked ${this._checkedPhones} phones`);
            this._receivedSalary += this._salary;
            console.log(`${Result.Success} ${Role.Guard} ${this.name} has received ${this._salary} dollars, totaly received ${this._receivedSalary}`);
            this._totalCheckedPhones += this._checkedPhones;
            console.log(`Currently ${Role.Guard} ${this.name} has checked ${this._totalCheckedPhones} phones in Total`);
            this._checkedPhones = 0;
        }
        else {
            console.log(`${Result.Failed} ${Role.Guard} ${this.name}, should check more than 2 phones to get paid`);
            console.log(`Currently ${Role.Guard} ${this.name} has checked ONLY ${this._checkedPhones} phones`);
        }
    }
    getPromotion() {
        if (this._totalCheckedPhones > 3 || this._foundPhones > 2) {
            console.log(`Currently ${Role.Guard} ${this.name} has checked ${this._totalCheckedPhones} phones in Total and found ${this._foundPhones}`);
            this._salary += 1000;
            console.log(`${Result.Success} ${Role.Guard} ${this.name} has been promoted, the current base salary is ${this._salary}`);
            this._totalCheckedPhones = 0;
            this._foundPhones = 0;
        }
        else {
            console.log(`${Result.Failed} ${Role.Guard} ${this.name} should check more than 3 phones in Total or find more than 2 phones to get promoted`);
            console.log(`Currently ${Role.Guard} ${this.name} has checked ONLY ${this._totalCheckedPhones} phones in Total and found ONLY ${this._foundPhones}`);
        }
    }
}
export { Guard };
