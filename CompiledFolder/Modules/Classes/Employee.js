// Abstract Class
class Employee {
    constructor(name, age, role, degree) {
        this.name = name;
        this.age = age;
        this.role = role;
        this.degree = degree;
    }
    getRole() {
        return this.role;
    }
    ;
    getDegree() {
        return this.degree;
    }
    ;
}
export { Employee };
