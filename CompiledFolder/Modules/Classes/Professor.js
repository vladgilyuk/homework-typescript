import { Role, Degree, Result } from '../enums.js';
import { Employee } from './Employee.js';
class Professor extends Employee {
    constructor(name, age, studyField) {
        super(name, age, Role.Professor, Degree.Doctorate);
        this.studyField = studyField;
        this.university = null;
        this._checkedExams = 0;
        this._totalCheckedExams = 0;
        this._raisedBestStudents = 0;
        this._salary = 2000;
        this._receivedSalary = 0;
    }
    checkMidExam(student) {
        if (student.university === this.university) {
            if (student.studyField === this.studyField) {
                if (student.midExamAnswer === null) {
                    console.log(`${Result.Failed} ${Role.Student} ${student.name} has not submitted an Exam yet`);
                }
                else if (student.midExamAnswer >= 8.5) {
                    student.midExam = true;
                    this._checkedExams++;
                    console.log(`${Result.Success} ${Role.Student} ${student.name} has Passed Mid exam. Checked by ${Role.Professor} ${this.name}`);
                }
                else {
                    student.midExam = false;
                    this._checkedExams++;
                    console.log(`${Result.Failed} ${Role.Student} ${student.name} has Failed Mid exam. Checked by ${Role.Professor} ${this.name}`);
                }
            }
            else {
                console.log(`${Result.Failed} Study Field mismatch`);
            }
        }
        else {
            console.log(`${Result.Failed} University mismatch`);
        }
    }
    checkFinalExam(student) {
        if (student.university === this.university) {
            if (student.studyField === this.studyField) {
                if (student.midExam) {
                    if (student.finalExamAnswer === null) {
                        console.log(`${Result.Failed} ${Role.Student} ${student.name} has not submitted an Exam yet`);
                    }
                    else if (student.finalExamAnswer >= 8.5) {
                        student.finalExam = true;
                        this._checkedExams++;
                        console.log(`${Result.Success} ${Role.Student} ${student.name} has Passed Final exam. Checked by ${Role.Professor} ${this.name}`);
                    }
                    else {
                        student.finalExam = false;
                        this._checkedExams++;
                        console.log(`${Result.Failed} ${Role.Student} ${student.name} has Failed Final exam. Checked by ${Role.Professor} ${this.name}`);
                    }
                }
                else {
                    console.log(`${Result.Failed} ${Role.Student} ${student.name}, should pass a MidExam at first`);
                }
            }
            else {
                console.log(`${Result.Failed} Study Field mismatch`);
            }
        }
        else {
            console.log(`${Result.Failed} University mismatch`);
        }
    }
    checkExtraExam(student) {
        if (student.university === this.university) {
            if (student.studyField === this.studyField) {
                if (student.midExam && student.finalExam) {
                    if (student.extraExamAnswer === null) {
                        console.log(`${Result.Failed} ${Role.Student} ${student.name} has not submitted an Exam yet`);
                    }
                    else if (student.extraExamAnswer === 10) {
                        student.extraExam = true;
                        this._checkedExams++;
                        this._raisedBestStudents++;
                        console.log(`${Result.Success} ${Role.Student} ${student.name} has Passed Extra exam. Checked by ${Role.Professor} ${this.name}`);
                    }
                    else {
                        student.extraExam = false;
                        this._checkedExams++;
                        console.log(`${Result.Failed} ${Role.Student} ${student.name} has Failed Extra exam. Checked by ${Role.Professor} ${this.name}`);
                    }
                }
                else {
                    console.log(`${Result.Failed} ${Role.Student} ${student.name}, should pass both Mid and Final exams at first`);
                }
            }
            else {
                console.log(`${Result.Failed} Study Field mismatch`);
            }
        }
        else {
            console.log(`${Result.Failed} University mismatch`);
        }
    }
    getSalary() {
        if (this._checkedExams > 2) {
            console.log(`Currently ${Role.Professor} ${this.name} has checked ${this._checkedExams} exams`);
            this._receivedSalary += this._salary;
            console.log(`${Result.Success} ${Role.Professor} ${this.name} has received ${this._salary} dollars, totaly received ${this._receivedSalary}`);
            this._totalCheckedExams += this._checkedExams;
            console.log(`Currently ${Role.Professor}  ${this.name} has checked ${this._totalCheckedExams} exams in Total`);
            this._checkedExams = 0;
        }
        else {
            console.log(`${Result.Failed} ${Role.Professor} ${this.name} should check more than 2 exams to get paid`);
            console.log(`Currently ${Role.Professor} ${this.name} has checked ONLY ${this._checkedExams} exams`);
        }
    }
    getPromotion() {
        if (this._totalCheckedExams > 3) {
            console.log(`Currently ${Role.Professor} ${this.name} has checked ${this._totalCheckedExams} exams in Total`);
            this._salary += 2000;
            this._totalCheckedExams = 0;
            console.log(`${Result.Success} ${Role.Professor} ${this.name} has been promoted, the current base salary is ${this._salary}`);
        }
        else {
            console.log(`${Result.Failed} ${Role.Professor} ${this.name} should check more than 3 exams in Total to get promoted`);
            console.log(`Currently ${Role.Professor} ${this.name} has checked ONLY ${this._totalCheckedExams} exams in Total`);
        }
    }
    getRaisedBestStudents() {
        return this._raisedBestStudents;
    }
}
export { Professor };
