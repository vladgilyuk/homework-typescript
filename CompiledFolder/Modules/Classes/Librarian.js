import { Role, Degree, Result } from '../enums.js';
import { Employee } from './Employee.js';
class Librarian extends Employee {
    constructor(name, age) {
        super(name, age, Role.Librarian, Degree.Master);
        this.university = null;
        this._givenBooks = 0;
        this._totalGivenBooks = 0;
        this._salary = 1500;
        this._receivedSalary = 0;
    }
    giveBook(student) {
        if (student.university === this.university) {
            if (student.librarianDebt > 0) {
                console.log(`${Result.Failed} ${Role.Student} ${student.name}, should pay the librarian debt at first to ${Role.Librarian} ${this.name}`);
            }
            else {
                student.books++;
                this._givenBooks++;
                console.log(`${Result.Success} ${Role.Student} ${student.name} has received a book. Given by ${Role.Librarian} ${this.name}`);
            }
        }
        else {
            console.log(`${Result.Failed} University mismatch`);
        }
    }
    getSalary() {
        if (this._givenBooks > 2) {
            console.log(`Currently ${Role.Librarian} ${this.name} has given ${this._givenBooks} books`);
            this._receivedSalary += this._salary;
            console.log(`${Result.Success} ${Role.Librarian} ${this.name} has received ${this._salary} dollars, totaly received ${this._receivedSalary}`);
            this._totalGivenBooks += this._givenBooks;
            console.log(`Currently ${Role.Librarian} ${this.name} has given ${this._totalGivenBooks} books in Total`);
            this._givenBooks = 0;
        }
        else {
            console.log(`${Result.Failed} ${Role.Librarian} ${this.name}, should give more than 2 books to get paid`);
            console.log(`Currently ${Role.Librarian} ${this.name} has given ONLY ${this._givenBooks} books`);
        }
    }
    getPromotion() {
        if (this._totalGivenBooks > 3) {
            console.log(`Currently ${Role.Librarian} ${this.name} has given ${this._totalGivenBooks} books in Total`);
            this._salary += 1000;
            console.log(`${Result.Success} ${Role.Librarian} ${this.name} has been promoted, the current base salary is ${this._salary}`);
            this._totalGivenBooks = 0;
        }
        else {
            console.log(`${Result.Failed} ${Role.Librarian}, ${this.name} should give more than 3 books in Total to get promoted`);
            console.log(`Currently ${Role.Librarian} ${this.name} has given ONLY ${this._totalGivenBooks} books in Total`);
        }
    }
}
export { Librarian };
