var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Role, Degree, Result } from '../enums.js';
import { CheckUniversityEnrollment } from '../decorators.js';
const getId = (() => {
    let counter = 0;
    return () => counter++;
})();
class Student {
    constructor(name, age, studyField, nationality, sex, university = null, midExamAnswer = null, finalExamAnswer = null, extraExamAnswer = null, books = 0, librarianDebt = 0, hasPhoneOnExam = false, foundPhoneOnExam = false, midExam = false, finalExam = false, extraExam = false, diploma = null, redDiploma = null, degree = Degree.None) {
        this.name = name;
        this.age = age;
        this.studyField = studyField;
        this.nationality = nationality;
        this.sex = sex;
        this.university = university;
        this.midExamAnswer = midExamAnswer;
        this.finalExamAnswer = finalExamAnswer;
        this.extraExamAnswer = extraExamAnswer;
        this.books = books;
        this.librarianDebt = librarianDebt;
        this.hasPhoneOnExam = hasPhoneOnExam;
        this.foundPhoneOnExam = foundPhoneOnExam;
        this.midExam = midExam;
        this.finalExam = finalExam;
        this.extraExam = extraExam;
        this.diploma = diploma;
        this.redDiploma = redDiploma;
        this.degree = degree;
    }
    passMidExam(answer) {
        this.midExamAnswer = answer;
        console.log(`${Result.Success} ${Role.Student} ${this.name} added Mid Exam answer: ${JSON.stringify(answer)}`);
    }
    passFinalExam(answer) {
        if (this.midExamAnswer > 0 && this.midExam === false) {
            console.log(`${Result.Failed} ${Role.Student} ${this.name}, should wait until the Mid exam is checked by ${Role.Professor}`);
        }
        else {
            if (this.midExam) {
                this.finalExamAnswer = answer;
                console.log(`${Result.Success} ${Role.Student} ${this.name} added Final Exam answer: ${JSON.stringify(answer)}`);
            }
            else {
                console.log(`${Result.Failed} ${Role.Student}  ${this.name}, should pass the Mid Exam first`);
            }
        }
    }
    passExtraExam(answer) {
        if (this.finalExamAnswer > 0 && this.finalExam === false) {
            console.log(`${Result.Failed} ${Role.Student} ${this.name}, should wait until the Final exam is checked by ${Role.Professor}`);
        }
        else {
            if (this.finalExam) {
                this.extraExamAnswer = answer;
                console.log(`${Result.Success} ${Role.Student} ${this.name} added Extra Exam answer: ${JSON.stringify(answer)}`);
            }
            else {
                console.log(`${Result.Failed} ${Role.Student} ${this.name}, should pass the Final Exam first`);
            }
        }
    }
    payBribe(professor) {
        const bribe = Math.random() < 0.5 ? "alcohol" : "money";
        console.log(`Bribe in a form of ${bribe} was suggested to ${Role.Professor} ${professor.name} by ${Role.Student} ${this.name}`);
        throw new Error(`${Result.Failed} ${Role.Student} ${this.name} should remember that bribe - is not an accepted way to pass an exam!`);
    }
    getDegree() {
        if (this.midExam && this.finalExam && this.extraExam) {
            if (this.degree === Degree.None) {
                this.degree = Degree.Bachelor;
            }
            else if (this.degree === Degree.Bachelor) {
                this.degree = Degree.Master;
            }
            else {
                this.degree = Degree.Doctorate;
            }
            this.redDiploma = {
                id: getId(),
                field: this.studyField,
                holder: this.name,
                specialNote: `${Role.Student} ${this.name} is an extremely talented ${Role.Student}`
            };
            this.midExam = false;
            this.finalExam = false;
            this.extraExam = false;
            this.midExamAnswer = null;
            this.finalExamAnswer = null;
            this.extraExamAnswer = null;
            console.log(`${Result.Success} ${Role.Student} ${this.name}, congratulations on obtaining a RED DIPLOMA in ${this.studyField} and obtaining a ${this.degree}`);
        }
        else if (this.midExam && this.finalExam) {
            if (this.degree === Degree.None) {
                this.degree = Degree.Bachelor;
            }
            else if (this.degree === Degree.Bachelor) {
                this.degree = Degree.Master;
            }
            else {
                this.degree = Degree.Doctorate;
            }
            this.diploma = {
                id: getId(),
                field: this.studyField,
                holder: this.name,
            };
            this.midExam = false;
            this.finalExam = false;
            this.extraExam = false;
            this.midExamAnswer = null;
            this.finalExamAnswer = null;
            this.extraExamAnswer = null;
            console.log(`${Result.Success} ${Role.Student} ${this.name}, congratulations on obtaining a diploma in ${this.studyField} and obtaining a ${this.degree}`);
        }
        else {
            console.log(`${Result.Failed} ${Role.Student} ${this.name}, should pass at least 2 exams(Mid & Final) in order to get a diploma and a degree`);
        }
    }
}
__decorate([
    CheckUniversityEnrollment
], Student.prototype, "passMidExam", null);
__decorate([
    CheckUniversityEnrollment
], Student.prototype, "passFinalExam", null);
__decorate([
    CheckUniversityEnrollment
], Student.prototype, "passExtraExam", null);
__decorate([
    CheckUniversityEnrollment
], Student.prototype, "payBribe", null);
__decorate([
    CheckUniversityEnrollment
], Student.prototype, "getDegree", null);
export { Student };
