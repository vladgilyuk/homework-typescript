var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Role, StudyField, Sex, Nationality, Result } from './Modules/enums.js';
import { logHire, logFire, logEnrollStudent, logKickStudent, logAddStudyField, logDeleteStudyField } from './Modules/decorators.js';
import { Professor } from './Modules/Classes/Professor.js';
import { Student } from './Modules/Classes/Student.js';
import { Librarian } from './Modules/Classes/Librarian.js';
import { Guard } from './Modules/Classes/Guard.js';
import { Rector } from './Modules/Classes/Rector.js';
//Generics
class University {
    constructor(name) {
        this.name = name;
        this.studyFields = [];
        this.students = [];
        this.employees = [];
        this.profesors = [];
        this.librarian = null;
        this.guard = null;
        this.rector = null;
    }
    addStudyFields(studyFields) {
        for (const studyField of studyFields) {
            let isStudyFieldExists = false;
            for (const field of this.studyFields) {
                if (field === studyField) {
                    isStudyFieldExists = true;
                    break;
                }
            }
            if (isStudyFieldExists) {
                console.log(`${Result.Failed} ${studyField} already exists in this university.`);
            }
            else {
                this.studyFields.push(studyField);
                console.log(`${Result.Success} ${studyField} has been added as a new Study Field.`);
            }
        }
    }
    enrollStudent(student, studyField) {
        if (this.studyFields.indexOf(studyField) !== -1) {
            this.students.push(student);
            student.university = this.name;
            console.log(`${Result.Success} ${Role.Student} ${student.name} has been enrolled`);
        }
        else {
            console.log(`${Result.Failed} ${Role.Student} ${student.name} should choose a different Study Field that is availaible in the university to get enrolled`);
        }
    }
    hireProfessor(professorCandidate, studyField) {
        if (this.studyFields.indexOf(studyField) !== -1) {
            this.profesors.push(professorCandidate);
            this.employees.push(professorCandidate);
            professorCandidate.university = this.name;
            console.log(`${Result.Success} ${Role.Professor} ${professorCandidate.name} has been hired`);
        }
        else {
            console.log(`${Result.Failed} ${Role.Professor} ${professorCandidate.name} should choose a different Study Field that is availaible in the university to get hired`);
        }
    }
    hireLibrarian(librarianCandidate) {
        if (this.librarian === null) {
            this.librarian = librarianCandidate;
            this.employees.push(this.librarian);
            this.librarian.university = this.name;
            console.log(`${Result.Success} ${Role.Librarian} ${librarianCandidate.name} has been hired`);
        }
        else {
            console.log(`${Result.Failed} Sorry ${Role.Librarian} ${librarianCandidate.name}, the position of ${Role.Librarian} has already been closed`);
        }
    }
    hireGuard(guardCandidate) {
        if (this.guard === null) {
            this.guard = guardCandidate;
            this.employees.push(this.guard);
            this.guard.university = this.name;
            console.log(`${Result.Success} ${Role.Guard} ${guardCandidate.name} has been hired`);
        }
        else {
            console.log(`${Result.Failed} Sorry ${Role.Guard} ${guardCandidate.name}, the position of ${Role.Guard} has already been closed`);
        }
    }
    hireRector(rectorCandidate) {
        if (this.rector === null) {
            this.rector = rectorCandidate;
            this.employees.push(this.rector);
            this.rector.university = this.name;
            console.log(`${Result.Success} ${Role.Rector} ${rectorCandidate.name} has been hired`);
        }
        else {
            console.log(`${Result.Failed} Sorry ${Role.Rector} ${rectorCandidate.name}, the position of ${Role.Rector} has already been closed`);
        }
    }
    deleteStudyField(studyField) {
        const index = this.studyFields.indexOf(studyField);
        if (index !== -1) {
            this.studyFields.splice(index, 1);
            console.log(`${Result.Success} Study Field ${studyField} has been deleted`);
        }
        else {
            console.log(`${Result.Failed} Study Field ${studyField} is not included in this university`);
        }
    }
    kickStudent(student) {
        const index = this.students.indexOf(student);
        if (index !== -1) {
            this.students.splice(index, 1);
            student.university = null;
            console.log(`${Result.Success} ${Role.Student} ${student.name} has been kicked`);
        }
        else {
            console.log(`${Result.Failed} ${Role.Student} ${student.name} is not enrolled in this university`);
        }
    }
    fireEmployee(employee) {
        const index = this.employees.indexOf(employee);
        if (index !== -1) {
            this.employees.splice(index, 1);
        }
    }
    fireProfessor(professor) {
        const index = this.profesors.indexOf(professor);
        if (index !== -1) {
            this.profesors.splice(index, 1);
            this.fireEmployee(professor);
            professor.university = null;
            console.log(`${Result.Success} ${Role.Professor} ${professor.name} has been fired`);
        }
        else {
            console.log(`${Result.Failed} ${Role.Professor} ${professor.name} is not working in this university`);
        }
    }
    fireLibrarian(librarian) {
        if (librarian === this.librarian) {
            const index = this.employees.indexOf(this.librarian);
            if (index !== -1) {
                this.employees.splice(index, 1);
                this.librarian = null;
                librarian.university = null;
                console.log(`${Result.Success} ${Role.Librarian} ${librarian.name} has been fired`);
            }
        }
        else {
            console.log(`${Result.Failed} ${Role.Librarian} ${librarian.name} is not working in this university`);
        }
    }
    fireGuard(guard) {
        if (this.guard === guard) {
            const index = this.employees.indexOf(this.guard);
            if (index !== -1) {
                this.employees.splice(index, 1);
                this.guard = null;
                guard.university = null;
                console.log(`${Result.Success} ${Role.Guard} ${guard.name} has been fired`);
            }
        }
        else {
            console.log(`${Result.Failed} ${Role.Guard} ${guard.name} is not working in this university`);
        }
    }
    fireRector(rector) {
        if (rector === this.rector) {
            const index = this.employees.indexOf(this.rector);
            if (index !== -1) {
                this.employees.splice(index, 1);
                this.rector = null;
                rector.university = null;
                console.log(`${Result.Success} ${Role.Rector} ${rector.name} has been fired`);
            }
        }
        else {
            console.log(`${Result.Failed} ${Role.Rector} ${rector.name} is not working in this university`);
        }
    }
    getStudyFieldsCount() {
        console.log(`Study Fields count: ${this.studyFields.length}`);
    }
    getStudentsCount() {
        console.log(`Students count: ${this.students.length}`);
    }
    getProfessorsCount() {
        console.log(`Professors count: ${this.profesors.length}`);
    }
    getEmployeesCount() {
        console.log(`Employees count: ${this.employees.length}`);
    }
}
__decorate([
    logAddStudyField
], University.prototype, "addStudyFields", null);
__decorate([
    logEnrollStudent
], University.prototype, "enrollStudent", null);
__decorate([
    logHire
], University.prototype, "hireProfessor", null);
__decorate([
    logHire
], University.prototype, "hireLibrarian", null);
__decorate([
    logHire
], University.prototype, "hireGuard", null);
__decorate([
    logHire
], University.prototype, "hireRector", null);
__decorate([
    logDeleteStudyField
], University.prototype, "deleteStudyField", null);
__decorate([
    logKickStudent
], University.prototype, "kickStudent", null);
__decorate([
    logFire
], University.prototype, "fireProfessor", null);
__decorate([
    logFire
], University.prototype, "fireLibrarian", null);
__decorate([
    logFire
], University.prototype, "fireGuard", null);
__decorate([
    logFire
], University.prototype, "fireRector", null);
// Testing our implementation
const student1 = new Student('Vlad', 24, StudyField.ComputerScience, Nationality.Ukrainian, Sex.Male);
const student2 = new Student('Andrzej', 22, StudyField.ComputerScience, Nationality.Polish, Sex.Male);
const student3 = new Student('Ben', 30, StudyField.Biology, Nationality.English, Sex.Male);
const student4 = new Student('Emily', 19, StudyField.Psychology, Nationality.French, Sex.Female);
const student5 = new Student('Otto', 22, StudyField.Economics, Nationality.German, Sex.Male);
const prof1 = new Professor('Taras', 29, StudyField.ComputerScience);
const prof2 = new Professor('Nazarii', 30, StudyField.Biology);
const prof3 = new Professor('Anastasia', 30, StudyField.Psychology);
const prof4 = new Professor('Vadym', 28, StudyField.Economics);
const prof5 = new Professor('Vitalyi', 32, StudyField.Physics);
const guard1 = new Guard("Will", 90);
const guard2 = new Guard("Sam", 85);
const librarian1 = new Librarian("Mary", 37);
const librarian2 = new Librarian("Jessica", 40);
const rector1 = new Rector("William", 65);
const rector2 = new Rector("Tomas", 57);
const university = new University("S-PRO University");
const universityPublic = new University("National University");
const universityPrivate = new University("Secrect University");
// University methods
university.addStudyFields([StudyField.ComputerScience, StudyField.Biology, StudyField.Physics]);
university.addStudyFields([StudyField.ComputerScience]); // F - (F - means it should be Failed)
university.getStudyFieldsCount();
university.addStudyFields([StudyField.Psychology, StudyField.Economics]);
university.getStudyFieldsCount();
university.deleteStudyField(StudyField.Economics);
university.deleteStudyField(StudyField.Economics); // F 
university.getStudyFieldsCount();
university.hireProfessor(prof1, prof1.studyField);
university.hireProfessor(prof2, prof2.studyField);
university.hireProfessor(prof3, prof3.studyField);
university.hireProfessor(prof4, prof4.studyField); // F 
university.hireProfessor(prof5, prof5.studyField);
university.getProfessorsCount();
university.fireProfessor(prof3);
university.fireProfessor(prof3); // F 
university.getProfessorsCount();
university.hireProfessor(prof3, prof3.studyField);
university.getProfessorsCount();
university.getEmployeesCount();
university.hireGuard(guard1);
university.hireGuard(guard2); // F 
university.getEmployeesCount();
university.hireLibrarian(librarian1);
university.hireLibrarian(librarian2); // F 
university.getEmployeesCount();
university.hireRector(rector1);
university.hireRector(rector2); // F 
university.getEmployeesCount();
university.fireGuard(guard1);
university.fireGuard(guard1); // F 
university.fireLibrarian(librarian1);
university.fireLibrarian(librarian2); // F 
university.fireRector(rector1);
university.fireRector(rector2); //F
university.getEmployeesCount();
university.hireGuard(guard1);
university.hireLibrarian(librarian1);
university.hireRector(rector1);
university.getEmployeesCount();
university.enrollStudent(student1, student1.studyField);
university.enrollStudent(student2, student2.studyField);
university.enrollStudent(student3, student3.studyField);
university.enrollStudent(student4, student4.studyField);
university.enrollStudent(student5, student5.studyField); //F
university.getStudentsCount();
university.kickStudent(student4);
university.kickStudent(student4); //F
university.getStudentsCount();
university.enrollStudent(student4, student4.studyField);
// Student methods + Professor methods
student1.passMidExam(10);
student2.passMidExam(7);
student3.passMidExam(7);
student4.passMidExam(5);
student5.passMidExam(4); //F
student4.passFinalExam(5); //F
student4.passExtraExam(5); //F
student4.getDegree(); // F 
try { // F 
    student4.payBribe(prof4);
}
catch (error) {
    console.log(error.message);
}
prof1.checkMidExam(student1);
prof1.checkMidExam(student2); //F
prof1.checkMidExam(student3); //F
prof4.checkMidExam(student1); //F
prof1.checkFinalExam(student2); //F
student1.passFinalExam(10);
prof1.checkFinalExam(student1);
student1.passExtraExam(10);
prof1.checkExtraExam(student1);
student1.getDegree();
student1.getDegree(); //F
prof1.getSalary();
prof1.getSalary(); // F 
prof1.getPromotion();
prof1.getPromotion(); // F 
//Librarian
librarian1.giveBook(student1);
librarian1.giveBook(student2);
librarian1.giveBook(student3);
librarian1.giveBook(student5); //F
student2.librarianDebt = 5; //F
librarian1.giveBook(student2); //F
student2.librarianDebt = 0;
librarian1.giveBook(student2);
librarian1.getSalary();
librarian1.getSalary(); //F
librarian1.getPromotion();
librarian1.getPromotion(); //F
librarian1.giveBook(student2);
librarian1.giveBook(student3);
librarian1.giveBook(student4);
librarian1.getSalary(); // example of increased base salary of 2500
// Guard methods
guard1.checkRandomPerson(student1);
guard1.checkRandomPerson(prof1); //F
guard1.checkRandomPerson(librarian1); //F
guard1.checkRandomPerson(rector1); //F
student2.hasPhoneOnExam = true;
student3.hasPhoneOnExam = true;
guard1.checkRandomPerson(student3);
guard1.checkRandomPerson(student2);
guard1.checkRandomPerson(student4);
guard1.getSalary();
guard1.getSalary(); //F
guard1.getPromotion();
guard1.getPromotion(); //F
//Rector methods
rector1.conductMeeting([prof1, prof2, prof3]);
rector1.conductMeeting([prof3, prof2, prof5]);
rector1.conductMeeting([prof1, prof2]);
rector1.conductMeeting([prof1, prof2, prof3, prof5, prof4]); // F
rector1.awardBestProfessor([prof1, prof2, prof3]);
rector1.awardBestProfessor([prof4, prof5, prof3]); // F
rector1.getSalary();
rector1.getSalary(); // F
rector1.getPromotion();
rector1.getPromotion(); // F
//Doctorate Speedrun
student1.passMidExam(10);
prof1.checkMidExam(student1);
student1.passFinalExam(10);
prof1.checkFinalExam(student1);
student1.passExtraExam(10);
prof1.checkExtraExam(student1);
student1.getDegree();
prof1.getSalary();
student1.passMidExam(10);
prof1.checkMidExam(student1);
student1.passFinalExam(10);
prof1.checkFinalExam(student1);
student1.passExtraExam(10);
prof1.checkExtraExam(student1);
student1.getDegree();
prof1.getSalary();
prof1.getPromotion();
